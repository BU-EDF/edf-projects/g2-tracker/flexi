EESchema Schematic File Version 2
LIBS:lshm-1-40-025-f-dv-n
LIBS:flex_edge_64
LIBS:power
LIBS:TestBeam_A-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LSHM-1-40-025-F-DV-N J1
U 1 1 54332C83
P 7500 3800
F 0 "J1" H 7500 1500 60  0000 C CNN
F 1 "LSHM-1-40-025-F-DV-N" V 7500 3650 60  0000 C CNN
F 2 "" H 7250 3850 60  0000 C CNN
F 3 "" H 7250 3850 60  0000 C CNN
	1    7500 3800
	1    0    0    -1  
$EndComp
$Comp
L FLEX_EDGE_64 J2
U 1 1 543475D9
P 950 4050
F 0 "J2" H 900 750 60  0000 C CNN
F 1 "FLEX_EDGE_64" V 950 4050 60  0000 C CNN
F 2 "" H 1200 4050 60  0000 C CNN
F 3 "" H 1200 4050 60  0000 C CNN
	1    950  4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1500 2200 6750 2200
Wire Wire Line
	1500 3700 6750 3700
Wire Wire Line
	1500 3800 1700 3800
Wire Wire Line
	1700 3800 1700 4000
Wire Wire Line
	1500 4000 2350 4000
Connection ~ 1700 4000
Wire Wire Line
	1500 3900 1700 3900
Connection ~ 1700 3900
Text Label 2350 4000 2    60   ~ 0
LVDS_shield_A
Wire Wire Line
	1500 3600 6750 3600
Wire Wire Line
	1500 3500 6750 3500
Wire Wire Line
	1500 3400 6750 3400
Wire Wire Line
	1500 3300 6750 3300
Wire Wire Line
	1500 3200 6750 3200
Wire Wire Line
	1500 3100 6750 3100
Wire Wire Line
	1500 3000 6750 3000
Wire Wire Line
	1500 2800 6750 2800
Wire Wire Line
	6750 2300 1500 2300
Wire Wire Line
	1500 2400 6750 2400
Wire Wire Line
	1500 2500 6750 2500
Wire Wire Line
	6750 2600 1500 2600
Wire Wire Line
	1500 2700 6750 2700
Wire Wire Line
	1500 2900 6750 2900
Wire Wire Line
	1500 4100 1700 4100
Wire Wire Line
	1700 4100 1700 4300
Wire Wire Line
	1500 4300 2350 4300
Connection ~ 1700 4300
Wire Wire Line
	1500 4200 1700 4200
Connection ~ 1700 4200
Text Label 2350 4300 2    60   ~ 0
LVDS_shield_B
Wire Wire Line
	6550 3800 6550 3900
Wire Wire Line
	5900 3900 6750 3900
Connection ~ 6550 3900
Wire Wire Line
	6750 3800 6550 3800
Connection ~ 6550 3800
Text Label 5900 3900 0    60   ~ 0
LVDS_shield_A
Wire Wire Line
	6550 4000 6550 4100
Wire Wire Line
	5900 4100 6750 4100
Connection ~ 6550 4100
Wire Wire Line
	6750 4000 6550 4000
Connection ~ 6550 4000
Text Label 5900 4100 0    60   ~ 0
LVDS_shield_B
Wire Wire Line
	6550 2000 6550 2100
Wire Wire Line
	5900 2100 6750 2100
Connection ~ 6550 2100
Wire Wire Line
	6750 2000 6550 2000
Connection ~ 6550 2000
Text Label 5900 2100 0    60   ~ 0
LVDS_shield_A
Wire Wire Line
	1500 4400 2600 4400
Wire Wire Line
	2600 4400 2600 4200
Wire Wire Line
	2600 4200 6750 4200
Wire Wire Line
	6750 4300 2700 4300
Wire Wire Line
	2700 4300 2700 4500
Wire Wire Line
	2700 4500 1500 4500
Wire Wire Line
	1500 4600 2800 4600
Wire Wire Line
	2800 4600 2800 4400
Wire Wire Line
	2800 4400 6750 4400
Wire Wire Line
	6750 4500 2900 4500
Wire Wire Line
	2900 4500 2900 4700
Wire Wire Line
	2900 4700 1500 4700
Wire Wire Line
	1500 4800 3000 4800
Wire Wire Line
	3000 4800 3000 4600
Wire Wire Line
	3000 4600 6750 4600
Wire Wire Line
	6750 4700 3100 4700
Wire Wire Line
	3100 4700 3100 4900
Wire Wire Line
	3100 4900 1500 4900
Wire Wire Line
	6750 4800 3200 4800
Wire Wire Line
	3200 4800 3200 5000
Wire Wire Line
	3200 5000 1500 5000
Wire Wire Line
	1500 5100 3300 5100
Wire Wire Line
	3300 5100 3300 4900
Wire Wire Line
	3300 4900 6750 4900
Wire Wire Line
	6750 5000 3400 5000
Wire Wire Line
	3400 5000 3400 5200
Wire Wire Line
	3400 5200 1500 5200
Wire Wire Line
	1500 5300 3500 5300
Wire Wire Line
	3500 5300 3500 5100
Wire Wire Line
	3500 5100 6750 5100
Wire Wire Line
	6750 5200 3600 5200
Wire Wire Line
	3600 5200 3600 5400
Wire Wire Line
	3600 5400 1500 5400
Wire Wire Line
	1500 5500 3700 5500
Wire Wire Line
	3700 5500 3700 5300
Wire Wire Line
	3700 5300 6750 5300
Wire Wire Line
	6750 5400 3800 5400
Wire Wire Line
	3800 5400 3800 5600
Wire Wire Line
	3800 5600 1500 5600
Wire Wire Line
	1500 5700 3900 5700
Wire Wire Line
	3900 5700 3900 5500
Wire Wire Line
	3900 5500 6750 5500
Wire Wire Line
	6750 5600 4000 5600
Wire Wire Line
	4000 5600 4000 5800
Wire Wire Line
	4000 5800 1500 5800
Wire Wire Line
	1500 5900 4100 5900
Wire Wire Line
	4100 5900 4100 5700
Wire Wire Line
	4100 5700 6750 5700
Wire Wire Line
	6550 5800 6550 5900
Wire Wire Line
	5900 5900 6750 5900
Connection ~ 6550 5900
Wire Wire Line
	6750 5800 6550 5800
Connection ~ 6550 5800
Text Label 5900 5900 0    60   ~ 0
LVDS_shield_B
Wire Wire Line
	1500 1900 1700 1900
Wire Wire Line
	1700 1900 1700 2100
Wire Wire Line
	2350 2100 1500 2100
Connection ~ 1700 2100
Wire Wire Line
	1500 2000 1700 2000
Connection ~ 1700 2000
Text Label 2350 2100 2    60   ~ 0
LVDS_shield_A
Wire Wire Line
	1500 6000 1700 6000
Wire Wire Line
	1500 6100 2350 6100
Connection ~ 1700 6100
Text Label 2350 6100 2    60   ~ 0
LVDS_shield_B
Wire Wire Line
	8450 2000 8450 2100
Wire Wire Line
	8250 2100 9100 2100
Connection ~ 8450 2100
Wire Wire Line
	8250 2000 8450 2000
Connection ~ 8450 2000
Text Label 9100 2100 2    60   ~ 0
LVDS_shield_A
Wire Wire Line
	8450 3800 8450 4100
Wire Wire Line
	8250 3900 9500 3900
Connection ~ 8450 3900
Wire Wire Line
	8250 3800 8450 3800
Connection ~ 8450 3800
Text Label 9100 3900 2    60   ~ 0
LVDS_shield_A
Wire Wire Line
	8450 4100 8250 4100
Wire Wire Line
	8250 4000 8450 4000
Connection ~ 8450 4000
Wire Wire Line
	8450 4200 8450 4500
Wire Wire Line
	8250 4300 9500 4300
Connection ~ 8450 4300
Wire Wire Line
	8250 4200 8450 4200
Connection ~ 8450 4200
Text Label 9100 4300 2    60   ~ 0
LVDS_shield_B
Wire Wire Line
	8450 4500 8250 4500
Wire Wire Line
	8250 4400 8450 4400
Connection ~ 8450 4400
Wire Wire Line
	8450 5800 8450 5900
Wire Wire Line
	8250 5900 9100 5900
Connection ~ 8450 5900
Wire Wire Line
	8250 5800 8450 5800
Connection ~ 8450 5800
Text Label 9100 5900 2    60   ~ 0
LVDS_shield_B
Text Label 2050 900  2    60   ~ 0
-3V
Wire Wire Line
	1500 900  2050 900 
Wire Wire Line
	1500 1000 1700 1000
Wire Wire Line
	1700 1000 1700 900 
Connection ~ 1700 900 
Text Label 2050 1100 2    60   ~ 0
-3Vret
Wire Wire Line
	1500 1200 1700 1200
Wire Wire Line
	1700 1200 1700 1100
Connection ~ 1700 1100
Text Label 2050 1300 2    60   ~ 0
+3V
Wire Wire Line
	1500 1300 2050 1300
Wire Wire Line
	1500 1400 1700 1400
Wire Wire Line
	1700 1400 1700 1300
Connection ~ 1700 1300
Text Label 2050 1500 2    60   ~ 0
+3Vret
Wire Wire Line
	1500 1600 1700 1600
Wire Wire Line
	1700 1600 1700 1500
Connection ~ 1700 1500
Text Label 8800 3400 2    60   ~ 0
+1.4V
Wire Wire Line
	8250 3400 8800 3400
Text Label 8800 3600 2    60   ~ 0
+1.4Vret
Wire Wire Line
	8250 3600 9500 3600
Text Label 2350 6300 2    60   ~ 0
TSTP1_IN
Wire Wire Line
	2350 6300 1500 6300
Text Label 2350 6400 2    60   ~ 0
TSTN1_IN
Wire Wire Line
	2350 6400 1500 6400
Text Label 2350 6500 2    60   ~ 0
TSTP2_IN
Wire Wire Line
	2350 6500 1500 6500
Text Label 2350 6600 2    60   ~ 0
TSTN2_IN
Wire Wire Line
	2350 6600 1500 6600
Text Label 2350 6800 2    60   ~ 0
TEMP_P
Wire Wire Line
	2350 6800 1500 6800
Wire Wire Line
	1500 6700 2650 6700
Text Label 9100 4600 2    60   ~ 0
DTHR
Wire Wire Line
	9100 4600 8250 4600
Text Label 9100 4700 2    60   ~ 0
IBLR
Wire Wire Line
	9100 4700 8250 4700
Text Label 9100 4800 2    60   ~ 0
TREFO_IN
Wire Wire Line
	9100 4800 8250 4800
Text Label 9100 4900 2    60   ~ 0
TREFE_IN
Wire Wire Line
	9100 4900 8250 4900
Text Label 8800 2200 2    60   ~ 0
-3V
Wire Wire Line
	8250 2200 8800 2200
Wire Wire Line
	8250 2300 8450 2300
Wire Wire Line
	8450 2200 8450 2400
Connection ~ 8450 2200
Text Label 8800 2500 2    60   ~ 0
-3Vret
Wire Wire Line
	8250 2500 9500 2500
Wire Wire Line
	8250 2600 8450 2600
Wire Wire Line
	8450 2500 8450 2700
Connection ~ 8450 2500
Wire Wire Line
	8450 2400 8250 2400
Connection ~ 8450 2300
Wire Wire Line
	8450 2700 8250 2700
Connection ~ 8450 2600
Text Label 8800 2800 2    60   ~ 0
+3V
Wire Wire Line
	8250 2800 8800 2800
Wire Wire Line
	8250 2900 8450 2900
Wire Wire Line
	8450 2800 8450 3000
Connection ~ 8450 2800
Text Label 8800 3100 2    60   ~ 0
+3Vret
Wire Wire Line
	8250 3100 9500 3100
Wire Wire Line
	8250 3200 8450 3200
Wire Wire Line
	8450 3100 8450 3300
Connection ~ 8450 3100
Wire Wire Line
	8450 3000 8250 3000
Connection ~ 8450 2900
Wire Wire Line
	8450 3300 8250 3300
Connection ~ 8450 3200
Wire Wire Line
	8250 3500 8450 3500
Wire Wire Line
	8450 3500 8450 3400
Connection ~ 8450 3400
Wire Wire Line
	8250 3700 8450 3700
Wire Wire Line
	8450 3700 8450 3600
Connection ~ 8450 3600
Text Label 2350 7100 2    60   ~ 0
DTHR
Wire Wire Line
	2350 6900 1500 6900
Text Label 2350 7200 2    60   ~ 0
IBLR
Wire Wire Line
	2350 7000 1500 7000
Text Label 2350 6900 2    60   ~ 0
TREFO_IN
Wire Wire Line
	2350 7100 1500 7100
Text Label 2350 7000 2    60   ~ 0
TREFE_IN
Wire Wire Line
	2350 7200 1500 7200
Text Label 9100 5100 2    60   ~ 0
TSTP1_IN
Wire Wire Line
	9100 5100 8250 5100
Text Label 9100 5200 2    60   ~ 0
TSTN1_IN
Wire Wire Line
	9100 5200 8250 5200
Text Label 9100 5300 2    60   ~ 0
TSTP2_IN
Wire Wire Line
	9100 5300 8250 5300
Text Label 9100 5400 2    60   ~ 0
TSTN2_IN
Wire Wire Line
	9100 5400 8250 5400
Text Label 9100 5500 2    60   ~ 0
TEMP_P
Wire Wire Line
	9100 5500 8250 5500
Wire Wire Line
	8250 5600 9500 5600
Text Label 9100 5000 2    60   ~ 0
Analog_shield
Wire Wire Line
	8250 5000 9500 5000
Text Label 9100 5700 2    60   ~ 0
testing_shield
Wire Wire Line
	8250 5700 9500 5700
Text Label 2050 1700 2    60   ~ 0
+1.4V
Wire Wire Line
	1500 1700 2050 1700
Text Label 2050 1800 2    60   ~ 0
+1.4Vret
Text Label 1700 2200 0    60   ~ 0
TDC_IN_0P
Text Label 1700 2300 0    60   ~ 0
TDC_IN_0N
Text Label 1700 2400 0    60   ~ 0
TDC_IN_1P
Text Label 1700 2500 0    60   ~ 0
TDC_IN_1N
Text Label 1700 2600 0    60   ~ 0
TDC_IN_2P
Text Label 1700 2700 0    60   ~ 0
TDC_IN_2N
Text Label 1700 2800 0    60   ~ 0
TDC_IN_3P
Text Label 1700 2900 0    60   ~ 0
TDC_IN_3N
Text Label 1700 3000 0    60   ~ 0
TDC_IN_4P
Text Label 1700 3100 0    60   ~ 0
TDC_IN_4N
Text Label 1700 3200 0    60   ~ 0
TDC_IN_5P
Text Label 1700 3300 0    60   ~ 0
TDC_IN_5N
Text Label 1700 3400 0    60   ~ 0
TDC_IN_6P
Text Label 1700 3500 0    60   ~ 0
TDC_IN_6N
Text Label 1700 3600 0    60   ~ 0
TDC_IN_7P
Text Label 1700 3700 0    60   ~ 0
TDC_IN_7N
Text Label 1700 4400 0    60   ~ 0
TDC_IN_8P
Text Label 1700 4500 0    60   ~ 0
TDC_IN_8N
Text Label 1700 4600 0    60   ~ 0
TDC_IN_9P
Text Label 1700 4700 0    60   ~ 0
TDC_IN_9N
Text Label 1700 4800 0    60   ~ 0
TDC_IN_10P
Text Label 1700 4900 0    60   ~ 0
TDC_IN_10N
Text Label 1700 5000 0    60   ~ 0
TDC_IN_11P
Text Label 1700 5100 0    60   ~ 0
TDC_IN_11N
Text Label 1700 5200 0    60   ~ 0
TDC_IN_12P
Text Label 1700 5300 0    60   ~ 0
TDC_IN_12N
Text Label 1700 5400 0    60   ~ 0
TDC_IN_13P
Text Label 1700 5500 0    60   ~ 0
TDC_IN_13N
Text Label 1700 5600 0    60   ~ 0
TDC_IN_14P
Text Label 1700 5700 0    60   ~ 0
TDC_IN_14N
Text Label 1700 5800 0    60   ~ 0
TDC_IN_15P
Text Label 1700 5900 0    60   ~ 0
TDC_IN_15N
$Comp
L GND #PWR?
U 1 1 543D613C
P 9500 5850
F 0 "#PWR?" H 9500 5850 30  0001 C CNN
F 1 "GND" H 9500 5780 30  0001 C CNN
F 2 "" H 9500 5850 60  0000 C CNN
F 3 "" H 9500 5850 60  0000 C CNN
	1    9500 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2500 9500 5850
Connection ~ 9500 5700
Wire Wire Line
	1500 1100 2050 1100
Wire Wire Line
	1500 1800 2050 1800
Wire Wire Line
	1500 1500 2050 1500
Connection ~ 9500 5000
Connection ~ 9500 4300
Connection ~ 9500 3900
Connection ~ 9500 3600
Connection ~ 9500 3100
Wire Wire Line
	1700 6000 1700 6100
Wire Wire Line
	1500 6200 2650 6200
Wire Wire Line
	2650 6200 2650 6800
Connection ~ 2650 6700
$Comp
L GND #PWR?
U 1 1 5491C1FF
P 2650 6800
F 0 "#PWR?" H 2650 6800 30  0001 C CNN
F 1 "GND" H 2650 6730 30  0001 C CNN
F 2 "" H 2650 6800 60  0000 C CNN
F 3 "" H 2650 6800 60  0000 C CNN
	1    2650 6800
	1    0    0    -1  
$EndComp
Connection ~ 9500 5600
$EndSCHEMATC
