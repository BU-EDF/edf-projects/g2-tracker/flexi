
Boston University P/N G2-Flexi_C Rev B

ZIF connector region stackup (left side of flexi in GERBERS)
*** NOTE *** THE BOTTOM LAYER MUST BE PEELED BACK (UNLAMINATED) 0.4 inches FROM THE EDGE
*** NOTE *** TO EXPOSE ZIF CONNECTOR PADS ON INNER 1 CU LAYER!

 - Top coverlay (1mil kapton, 1mil adhesive)
 - Front Cu (0.5 oz) Flex base (3 mil kapton)
 - Adhesive (1 mil)
   / Inner 2 Cu (0.5 oz)
 - Flex base (5 mil)
   \ Inner 1 Cu (0.5 oz)
 - Adhesive (1 mil)                            *** PEELED AWAY 0.4" FROM EDGE ***
 - Front Cu (0.5 oz) Flex base (3 mil kapton)  *** PEELED AWAY 0.4" FROM EDGE ***
 - Top coverlay (1mil kapton, 1mil adhesive)   *** PEELED AWAY 0.4" FROM EDGE ***


SMD connector region stackup (upper-right side of flexi in GERBERS)
SMD connector on bottom Cu layer requires stiffener on top side (1.2" by 0.36")

 - Stiffener (20 mil)
 - Top coverlay (1mil kapton, 1mil adhesive)
 - Front Cu (0.5 oz) Flex base (3 mil kapton)
 - Adhesive (1 mil)
   / Inner 2 Cu (0.5 oz)
 - Flex base (5 mil)
   \ Inner 1 Cu (0.5 oz)
 - Adhesive (1 mil)
 - Front Cu (0.5 oz) Flex base (3 mil kapton)
 - Top coverlay (1mil kapton, 1mil adhesive)

Attached files:
g2_Flexi_C_revB-B_Cu.pho
g2_Flexi_C_revB-B_Mask.pho
g2_Flexi_C_revB-B_Paste.pho
g2_Flexi_C_revB-B_SilkS.pho
g2_Flexi_C_revB.drl
g2_Flexi_C_revB-drl_map.pho
g2_Flexi_C_revB-Edge_Cuts.pho
g2_Flexi_C_revB-Fab_dwg.pho
g2_Flexi_C_revB-F_Cu.pho
g2_Flexi_C_revB-F_Mask.pho
g2_Flexi_C_revB-F_Paste.pho
g2_Flexi_C_revB-F_SilkS.pho
g2_Flexi_C_revB-Inner1_Cu.pho
g2_Flexi_C_revB-Inner2_Cu.pho
g2_Flexi_C_revB-NPTH.drl
g2_Flexi_C_revB-NPTH-drl_map.pho

